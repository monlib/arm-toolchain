import os
from conans import ConanFile
from conans import tools

class ArmtoolchainConan(ConanFile):
    name = "arm-toolchain"
    version = "r47"
    # TODO: Windows needs to run an .exe for extracting
    settings = { 'os_build': ['Linux', 'Macos'], 'arch_build': ['x86', 'x86_64'] }
    description = 'ARM toolchain for use in Monlib. Install devkitARM properly for your own stuff.'
    url = 'https://gitlab.com/monlib/arm-toolchain.git'
    homepage = 'https://sourceforge.net/projects/devkitpro/files/devkitARM/'
    license = 'GPL'

    def genSuffix(self):
        return 'x86_64-{platform}.tar.bz2'.format(
            platform="osx" if self.settings.os_build == 'Macos' else 'linux'
        )

    def build(self):
        root_url = 'https://sourceforge.net/projects/devkitpro/files/devkitARM/'
        version_part = 'devkitARM_{0}/'.format(self.version)
        filename = 'devkitARM_{release}-{suffix}'.format(
            release=self.version,
            suffix=self.genSuffix()
        )
        download_suffix = '/download'
        url = root_url + version_part + filename + download_suffix
        tools.download(url, 'devkit.tar.bz2')
        tools.unzip('devkit.tar.bz2', keep_permissions=True)

    def package(self):
        self.copy("*", dst="", src="devkitARM")

    def package_info(self):
        bin_dir = os.path.join(self.package_folder, 'bin')
        def binary(name):
            return os.path.join(bin_dir, name)
        self.env_info.path.append(bin_dir)
        self.env_info.AR = binary('arm-none-eabi-ar')
        self.env_info.AS = binary('arm-none-eabi-as')
        self.env_info.CC = binary('arm-none-eabi-gcc')
        self.env_info.CXX = binary('arm-none-eabi-g++')
        self.env_info.NM = binary('arm-none-eabi-nm')
        self.env_info.RANLIB = binary('arm-none-eabi-ranlib')
        self.env_info.SYSROOT = self.package_folder
        self.env_info.DEVKITARM = self.package_folder
