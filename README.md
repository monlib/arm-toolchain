# arm-toolchain

This is a conanfile for packaging devkitARM. This project is for internal use
by Monlib and Monman only; it is not associated with the developers or owners
of devkitARM. Please report all issues here instead of bothering the
devkitPro team.

The MIT license applies to the files in this repo, devkitARM itself is GPL.
